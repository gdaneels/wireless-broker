# selvie-wirelessbroker

### How to run?
python run.py

### Contents
* WB.py contains the RESTful endpoints implementation.  
* WBSocketServer.py contains the WebSocket implementation  
* run.py combines the RESTful and WebSocket interface  
* websocket.html can be used to communicate as a client/UE (over WebSockets) with the wireless broker  

### IPs
* Restful IP: http://localhost:5000/
* WebSocket IP: ws://localhost:8001/

### Device Registration
To register a device at the wireless broker, send following JSON (and replace the values) to the wireless broker after connecting:
{"message": "device_registration", "client_id": "7a:31:c1:2f:5b:00", "cameraResolution": "800x600", "cpu": "2.4GHz", "user-agent": "foo"}