# SELVIE project
# This module contains all methods related to contacting the Meru controller (information).
# @author Glenn Daneels

import logging
import settings
import paramiko
import socket
import requests
import json
from urllib2 import urlopen
import WBWebSocket as wb

location = {}
location_attempts = {}

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("WirelessBroker")
clientLogger = logging.getLogger("ClientOverview")

# Log the locations of all the registered clients.
def log_clients_locations_overview():
    clientLogger.info("Location of registered clients: %s.", len(wb.socket_clienthandlers))
    if len(wb.socket_clienthandlers) > 0:
        clients = ""
        for client, val in wb.socket_clienthandlers.iteritems():
            if client in location:
                clients = clients + "client = " + client + ", AP = " + str(location[client]) + ", attempts = " + str(location_attempts[client]) + "\r\n";
            else:
                clients = clients + client + ": unknown location (?)";
        clientLogger.info(clients)

# Sets up the connection to the Meru Controller.
# @return Returns the ssh connection object if successful, otherwise the None object.
def setup_ssh_connection():
    logger.info("Enter: Setting up the ssh connection (user = %s, passw = %s, address = %s).", settings.controller["username"], settings.controller["password"], settings.ip["controller"])
    # Set up the ssh call.
    try:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(settings.ip["controller"], username=settings.controller["username"], password=settings.controller["password"], timeout=settings.wb["max_ssh_timeout"])
        logger.info("Exit: Setting up the ssh connection (user = %s, passw = %s, address = %s).", settings.controller["username"], settings.controller["password"], settings.ip["controller"])
        return ssh
    except:
        logger.exception("Exit: could not set up ssh connection (user = %s, passw = %s, address = %s).", settings.controller["username"], settings.controller["password"], settings.ip["controller"])
        return None
    logger.error("Exit: could not set up ssh connection (user = %s, passw = %s, address = %s).", settings.controller["username"], settings.controller["password"], settings.ip["controller"])
    return None

# Fetch periodically the locations of all the connected clients via the Meru controller.
# @param ssh The ssh connection object that is periodically used for fetching the locations.
def fetch_locations(ssh):
    logger.info("Enter: Fetch each client's location.")
    try:
        if len(wb.socket_clienthandlers) > 0:
            # Using the Meru APs
            if ssh is not None:
                # Get the necessary information.
                stdin, stdout, stderr = ssh.exec_command("show station", timeout=settings.wb["max_ssh_timeout"])
                lines = stdout.readlines()
                stdin_ap, stdout_ap, stderr_ap = ssh.exec_command("show ap", timeout=settings.wb["max_ssh_timeout"])
                lines_ap = stdout_ap.readlines()

                # Update all the information.
                for client_id, val in wb.socket_clienthandlers.iteritems():
                    # If the client_id is not yet in the location attempts, initiliaze it.
                    if (client_id not in location_attempts.keys()):
                        location_attempts[client_id] = settings.wb["max_location_attempts"]
                    # here should also come something for the location transmits.
                    found = False
                    for line in lines:
                        splitted = line.split(" ")
                        # If the station is found in the output of the show station command.
                        if client_id == splitted[0]:
                            for line_ap in lines_ap:
                                splitted_ap = line_ap.split(" ")
                                # If the ap id and ap name are found in the output of the "show ap" command.
                                if splitted_ap[0] == splitted[8] and splitted_ap[5] == splitted[12]:
                                    # Save the location.
                                    if client_id in location.keys() and location[client_id] == splitted_ap[21]:
                                        # If the location is still the same, make this clear by setting to -1.
                                        # This way, the update knows no update should be sent.
                                        location_attempts[client_id] = settings.wb["max_location_attempts"]
                                        logger.info("Found identical location %s for client_id %s (attempts set to %s).", splitted_ap[21], client_id, location_attempts[client_id])
                                    else:
                                        # If the situation changed, make this clear by setting to 0.
                                        location[client_id] = splitted_ap[21]
                                        location_attempts[client_id] = settings.wb["max_location_attempts"]
                                        logger.info("Found new location %s for client_id %s (attempts reset to %s)", splitted_ap[21], client_id, location_attempts[client_id])
                                    found = True
                    # If the station or AP is not found at all, reset the location and add an attempt to the counter.
                    if not found:
                        if location[client_id] == settings.wb["unknown_mac_location"]:
                            location_attempts[client_id] = location_attempts[client_id] - 1
                        else:
                            location[client_id] = settings.wb["unknown_mac_location"]
                            location_attempts[client_id] = location_attempts[client_id] - 1
                        logger.info("Did not found location for client id %s (attempts: %s).", client_id, location_attempts[client_id])
                logger.info("Exit: Fetch each client's location.")
            else:
                logger.error("Exit: Fetch each client's location - could not set up ssh connection.")
                return 2
        else:
            logger.info("Exit: Fetch each client's location (there were no clients).")
    except paramiko.ssh_exception.SSHException as detail:
        logger.error("Exit: Fetch each client's location - paramiko exception ssh. %s", detail)
        return 2
    except socket.error as detail:
        logger.error("Exit: Fetch each client's location - socket error ssh. %s", detail)
        return 2
    except Exception as detail:
        logger.error("Exit: Fetch each client's location - an unknown exception.")
        return 2 # we return error code 2, because we suspect a ssh paramiko exception here. What else could be going wrong?
    except:
        logger.error("Exit: Fetch each client's location - an unknown error.")
        return 2 # we return error code 2, because we suspect a ssh paramiko exception here. What else could be going wrong?
    return 0

def cleanup_clients():
    try:
        logger.info("Enter: clean all clients who are not reachable anymore.")
        count = 0
        for client_id in location_attempts.keys():
            if location_attempts[client_id] == 0:
                count = count + 1
                wb.socket_clienthandlers[client_id].sendClose()
                if client_id in location.keys():
                    del location[client_id]
                if client_id in location_attempts.keys():
                    del location_attempts[client_id]
                if client_id in wb.socket_clienthandlers.keys():
                    del wb.socket_clienthandlers[client_id]
        logger.info("Exit: clean all clients who are not reachable anymore (%s cleaned).", count)
    except Exception as detail:
        logger.exception("cleaning did go wrong. %s", detail)

# Periodically send location updates on all the registered clients to the UGC via a RESTful call.
def send_location_updates():
    logger.info("Enter: Send location updates to the UGC.")
    if (len(location) > 0):
        try:
            addr = settings.ip["ugc"] + "/v1/location"
            location_list = [{k:v} for k, v in location.iteritems()]
            locations = json.dumps(location_list)
            payload = {'locations': locations}
            headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
            r = requests.put(addr, data=json.dumps(payload), headers=headers, timeout=settings.wb["max_request_timeout"])
            json_response = r.json()
            if (r.status_code != 200):
                raise Exception("Status code of call is NOT 200, but " + str(r.status_code))
            if ('status' not in json_response or 'response' not in json_response or type(json_response["response"]) is not dict):
               raise Exception("Response of forward location RESTFUL had the wrong syntax: " + str(json_response))
            if (json_response["status"] != 'OK'):
               raise Exception("Response of confirm request RESTFUL contained a status NOT OK: " + str(json_response))
            logger.info("Exit: Send location updates to the UGC.")
        except Exception as detail:
            logger.exception("Exit: Send location updates - could not send location updates to UGC (%s): %s.", settings.ip["ugc"], detail)
    if (len(location) == 0):
        logger.info("Exit: Send location updates to the UGC cause of no clients.")

# Helperfunction to combine the fetching, sending and logging of al the locations of the registered clients.
# @param ssh The ssh connection object to be used to periodically send the location updates.
def update_locations(ssh):
    try:
        logger.info("Enter: update locations of all clients [PERIODICALLY]")
        # First, fetch the locations of all known clients.
        code = fetch_locations(ssh)
        if code == 2: # if the fetching did not succeed because of a ssh error
            log_clients_locations_overview()
            logger.info("Exit: update locations of all clients -- ssh error [PERIODICALLY]")
            return code
        # This function will check if the attempts of getting certain clients exceed the limit. If so, close their connection.
        cleanup_clients()
        # Now, send the updates to the UGC.
        send_location_updates()
        log_clients_locations_overview()
        logger.info("Exit: update locations of all clients [PERIODICALLY]")
        return code
    except:
        logger.error("Exit: update locations of all clients [PERIODICALLY] - an unknown error.")

