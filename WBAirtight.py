# SELVIE project
# Wireless Broker implementation
# @author Glenn Daneels

from flask import Flask, request, abort, jsonify
from flask.ext.restful import Resource, Api
from SimpleWebSocketServer import WebSocket, SimpleWebSocketServer
import logging
import random
import thread
import json
import paramiko
import requests
import settings

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("WirelessBroker")

# Default Flask stuff.
app = Flask(__name__)
# Default Flask Restful stuff.
api = Api(app)

def make_error(status, message, fName, payload="none"):
    response = jsonify({
        'status': status,
        'message': message
    })
    response.status = status
    logger.error("In function %s: message \"%s\" and status %s (payload %s).", fName, message, status, payload)
    return response, 200

# intelligence
# types: string, int, datetime

socket_clienthandlers = {}
location = {}
location_attempts = {}
#location_transmits = {}

clientLogger = logging.getLogger("ClientOverview")

def clienthandler_exist(macaddress):
    # socket_clienthandlers[macaddress] = handler
    if macaddress in socket_clienthandlers:
        logger.info("In function clienthandler_exist: client handler for mac %s is already registered.", macaddress)
        return True
    else:
        logger.info("In function clienthandler_exist: client handler for mac %s not registered.", macaddress)
        return False

def log_clients_locations_overview():
    clientLogger.info("Location of registered clients: %s.", len(socket_clienthandlers))
    if len(socket_clienthandlers) > 0:
        clients = ""
        for client, val in socket_clienthandlers.iteritems():
            if client in location:
                clients = clients + "client = " + client + ", AP = " + str(location[client]) + ", attempts = " + str(location_attempts[client]) + "\r\n";
            else:
                clients = clients + client + ": unknown location (?)";
        clientLogger.info(clients)

################ FROM UGC ##################

# Succesfull query
# curl -H "Content-Type: application/json" http://localhost:8001/v1/content/request -X POST -d '{"request_id": "1", "client_id": "7a:31:c1:2f:5b:00", "content_id": "3", "contentStartTime": 1429521187492, "contentEndTime": 1429521187492, "deadlinePolicy": "deadlinePolicy", "deadline": "deadline"}' -v
# curl -H "Content-Type: application/json" http://10.10.131.194:5000/v1/content/request -X POST -d '{"request_id": 1, "client_id": "f8:a9:d0:8a:a8:8c", "content_id": 3, "contentStartTime": "contentStartTime", "contentEndTime": "contentEndTime", "deadlinePolicy": "deadlinePolicy", "deadline": "deadline"}' -v
# Faulty JSON data: curl -H "Content-Type: application/json" http://localhost:5000/v1/content/request -X POST -d '{"request_id": 1, "client_id": "7a:31:c1:2f:5b:00", "content_id": 3, "contentStartTime": "contentStartTime", "contentEndTime": "contentEndTime" "deadlinePolicy": "deadlinePolicy", "deadline": "deadline"}' -v
@app.route('/v1/content/request', methods=['POST'])
def content_request():
    logger.info("Enter: (from UGC) content request call.")
    if not request.json:
        return make_error('400', 'no json', 'content_request')
    if 'request_id' not in request.json or type(request.json['request_id']) is not unicode:
        return make_error('400', 'request_id fault', 'content_request', request.json)
    if 'client_id' not in request.json or type(request.json['client_id']) is not unicode:
        return make_error('400', 'client_id fault', 'content_request', request.json)
    if 'content_id' not in request.json or type(request.json['content_id']) is not unicode:
        return make_error('400', 'content_id fault', 'content_request', request.json)
    if 'contentStartTime' not in request.json or type(request.json['contentStartTime']) is not int:
        return make_error('400', 'contentStartTime fault', 'content_request', request.json)
    if 'contentEndTime' not in request.json or type(request.json['contentEndTime']) is not int:
    	return make_error('400', 'contentEndTime fault', 'content_request', request.json)
    if 'deadlinePolicy' not in request.json or type(request.json['deadlinePolicy']) is not unicode:
    	return make_error('400', 'deadlinePolicy fault', 'content_request', request.json)
    if 'deadline' not in request.json or type(request.json['deadline']) is not unicode:
    	return make_error('400', 'deadline fault', 'content_request', request.json)

    logger.info("(from UGC) CONTENT REQUEST call for client_id %s, request_id %s and content_id %s.", request.json['client_id'], request.json['request_id'], request.json['content_id'])

    if (clienthandler_exist(request.json['client_id'])):
        fwd_content_request(request.json)
    else:
        return make_error('400', 'no socket connection', 'content_request', request.json)
    
    logger.info("Exit: (from UGC) content request call.")
    return jsonify({'status': "OK", 'estimatedArrivalTime': 1}), 200

    # Succesfull query
# curl -H "Content-Type: application/json" http://localhost:5000/v1/content/request/1 -X DELETE -d '{"client_id": "7a:31:c1:2f:5b:00", "request_id": "1"}' -v
# curl -H "Content-Type: application/json" http://192.168.1.129:5000/v1/content/request/1 -X DELETE -d '{"client_id": "30:75:12:b5:3c:c5", "request_id": "1"}' -v
@app.route('/v1/content/request/<int:request_id>', methods=['DELETE'])
def content_cancel(request_id):
    logger.info("Enter: (from UGC) content cancel call.")
    if not request.json:
        return make_error('400', 'no json', 'content_cancel')
    if 'client_id' not in request.json or type(request.json['client_id']) is not unicode:
        return make_error('400', 'client_id', 'content_cancel', request.json)
    if 'request_id' not in request.json or type(request.json['request_id']) is not unicode:
        return make_error('400', 'request_id', 'content_cancel', request.json)

    logger.info("(from UGC) CONTENT CANCEL call for client_id %s and request_id %s.", request.json['client_id'], request.json['request_id'])

    if (clienthandler_exist(request.json['client_id'])):
        fwd_content_cancel(request.json)
    else:
        return make_error('400', 'no socket connection', 'content_cancel', request.json)

    logger.info("Exit: (from UGC) content cancel call.")
    return jsonify({'status': 'OK'}), 200


# Succesfull query
# curl -H "Content-Type: application/json" http://127.0.0.1:5000/v1/configuration/30:75:12:b5:3c:c5 -X POST -d '{"commands": [{"resolution":"800x600"}]}' -v
@app.route('/v1/configuration/<string:client_id>', methods=['POST'])
def device_reconfiguration(client_id):
    logger.info("Enter: (from UGC) device reconfiguration call.")
    if not request.json:
        return make_error('400', 'no json', 'device_reconfiguration', request.json)
    if 'commands' not in request.json or type(request.json['commands']) is not list:
        return make_error('400', 'commands fault', 'device_reconfiguration', request.json)

    logger.info("(from UGC) DEVICE RECONFIGURATION call for client_id %s.", client_id)


    #
    #
    # Potentially in the future, this should support client_id as an array.
    #
    #

    if (clienthandler_exist(client_id)):
        fwd_device_reconfig(request.json, client_id)
    else:
        return make_error('400', 'no socket connection', 'device_reconfiguration', request.json)

    logger.info("Exit: (from UGC) device reconfiguration call.")
    return jsonify({'status': 'OK'}), 200

################ FROM UE ##################

# Succesfull query
# curl -H "Content-Type: application/json" http://192.168.1.129:5000/v1/device/register -X POST -d '{"MAC": "7a:31:c1:2f:5b:00", "cameraResolution": "800x600", "cpu": "2.4GHz", "user-agent": "foo"}' -v
# @app.route('/v1/device/register', methods=['POST'])
# def device_register():
#     if not request.json:
#         return make_error('400', 'no json')
#     if 'client_id' not in request.json or type(request.json['client_id']) is not unicode:
#         return make_error('400', 'client_id fault')
#     if 'cameraResolution' not in request.json or type(request.json['cameraResolution']) is not unicode:
#         return make_error('400', 'cameraResolution fault')
#     if 'cpu' not in request.json or type(request.json['cpu']) is not unicode:
#         return make_error('400', 'cpu fault')
#     if 'user-agent' not in request.json or type(request.json['user-agent']) is not unicode:
#         return make_error('400', 'user-agent fault')

#     # Check if the MAC / device is already registered.
#     # if not already_registered_mac(request.json['MAC']):
#     #      mac_clientids.append(request.json['MAC'])      
#     # else:
#     #     return make_error('400', 'DUPLICATE REGISTRATION')


#     return jsonify({'status': 'OK', 'response': {'ugc': UGC_ADDRESS_PORT}}), 200

# Succesfull query
# curl -H "Content-Type: application/json" http://192.168.1.129:5000/v1/device/parameter/7a%3A31%3Ac1%3A2f%3A5b%3A00 -X PUT -d '{"name": "goodput", "value": "54.0", "unit": "Mbps", "aggregationType": "max", "aggregationRangeStart": "datetime", "aggregationRangeEnd": "datetime"}' -v
@app.route('/v1/device/parameter/<string:client_id>', methods=['PUT'])
def device_parameter(client_id):
    logger.info("Enter: (from UE %s) device parameter update call.", client_id)
    if not request.json:
        return make_error('400', 'no json', 'device_parameter', request.json)
    if 'name' not in request.json or type(request.json['name']) is not unicode:
        return make_error('400', 'name fault', 'device_parameter', request.json)
    if 'value' not in request.json or type(request.json['value']) is not unicode:
        return make_error('400', 'value fault', 'device_parameter', request.json)
    if 'unit' not in request.json or type(request.json['unit']) is not unicode:
        return make_error('400', 'unit fault', 'device_parameter', request.json)
    if 'aggregationType' not in request.json or type(request.json['aggregationType']) is not unicode:
        return make_error('400', 'aggregationType fault', 'device_parameter', request.json)
    if 'aggregationRangeStart' not in request.json or type(request.json['aggregationRangeStart']) is not unicode:
        return make_error('400', 'aggregationRangeStart fault', 'device_parameter', request.json)
    if 'aggregationRangeEnd' not in request.json or type(request.json['aggregationRangeEnd']) is not unicode:
        return make_error('400', 'aggregationRangeEnd fault', 'device_parameter', request.json)

    logger.info("(from UE) DEVICE PARAMETER call for client_id %s.", client_id)

    logger.info("Exit: (from UE %s)  device parameter update call.", client_id)
    return jsonify({'status': 'OK'}), 200

###### BEGIN - These are dummy interfaces to test the functionality of the WB ######

@app.route('/v1/location', methods=['POST'])
def location_post():
    logger.info("Enter: location post call.")

    if not request.json:
        return make_error('400', 'no json', 'location_update')
    if 'client_id' not in request.json or type(request.json['client_id']) is not unicode:
        return make_error('400', 'client_id fault', 'location_update', request.json)
    if 'ap_id' not in request.json or type(request.json['ap_id']) is not unicode:
        return make_error('400', 'ap_id fault', 'location_update', request.json)

    logger.info("Location post call for client_id %s and ap_id %s.", request.json['client_id'], request.json['ap_id'])

    logger.info("Exit: location post call for client_id %s.", request.json['client_id'])
    return jsonify({'status': 'OK', 'response': {}}), 200

@app.route('/v1/location/<string:client_id>', methods=['PUT'])
def location_update(client_id):
    logger.info("Enter: location update call for client_id %s.", client_id)
    if not request.json:
        return make_error('400', 'no json', 'location_update')
    if 'client_id' not in request.json or type(request.json['client_id']) is not unicode:
        return make_error('400', 'client_id fault', 'location_update', request.json)
    if 'ap_id' not in request.json or type(request.json['ap_id']) is not unicode:
        return make_error('400', 'ap_id fault', 'location_update', request.json)

    logger.info("Location post call for client_id %s and ap_id %s.", request.json['client_id'], request.json['ap_id'])

    logger.info("Exit: location update for client_id %s.", request.json['client_id'])
    return jsonify({'status': 'OK', 'response': {}}), 200

@app.route('/v1/request/confirmation', methods=['POST'])
def content_request_confirmation():
    logger.info("Enter: content confirmation call.")
    if not request.json:
        return make_error('400', 'no json', 'content_request_confirmation')
    if 'request_id' not in request.json or type(request.json['request_id']) is not unicode:
        return make_error('400', 'request_id fault', 'content_request_confirmation', request.json)
    if 'content_id' not in request.json or type(request.json['content_id']) is not unicode:
        return make_error('400', 'content_id fault', 'content_request_confirmation', request.json)

    logger.info("Content request confirmation call for request_id %s and content_id %s.", request.json['request_id'], request.json['content_id'])

    logger.info("Exit: content confirmation call for request id %s and content_id %s.", request.json['request_id'], request.json['content_id'])
    return jsonify({'status': 'OK', 'response': {}}), 200


###### END - These are dummy interfaces to test the functionality of the WB ######


################ WEBSOCKET related DEVICE REGISTRATION ##################

# Websocket Device Registration.
# Called from WBSocketServer to register a new device.
# def websocket_device_register(payload, socketHandler):
#     dict_payload = json.loads(payload.decode("utf8"))
#     logger.info("Enter: websocket device registration call for %s", dict_payload['client_id'])

#     # Add the socket handler.
#     socket_clienthandlers[dict_payload['client_id']] = socketHandler

#     logger.info("Exit: websocket device registration call for %s", dict_payload['client_id'])
#     # All was well, send the success response.
#     return "{\"message\": \"device_registration\", \"status\": \"OK\", \"response\": {\"ugc\": \"%s\", \"director\": \"%s\"}}" % (settings.ip["ugc"], settings.ip["director"])

################ TO UGC ##################

def setup_ssh_connection():
    logger.info("Enter: Setting up the ssh connection (user = %s, passw = %s).", settings.cloudcontroller["username"], settings.cloudcontroller["password"])
    # Set up the ssh call.
    try:
        ssh = []
        for ap in settings.aps:
            ssh.append(paramiko.SSHClient()) 
            ssh[-1].set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh[-1].connect(ap, username=settings.cloudcontroller["username"], password=settings.cloudcontroller["password"])
            logger.info("Set up connection to ap %s (user = %s, passw = %s).", ap, settings.cloudcontroller["username"], settings.cloudcontroller["password"])
        logger.info("Exit: Setting up the ssh connection (user = %s, passw = %s, address = %s).", settings.cloudcontroller["username"], settings.cloudcontroller["password"], ap)
        return ssh
    except:
        logger.exception("Exit: could not set up ssh connection (user = %s, passw = %s, address = %s).", settings.cloudcontroller["username"], settings.cloudcontroller["password"])
        return None
    logger.error("Exit: could not set up ssh connection (user = %s, passw = %s).", settings.cloudcontroller["username"], settings.cloudcontroller["password"])
    return None

# Get the ap id and name based on the mac adres
def get_ap_id_name(ssh, mac):
    logger.info("Enter: Getting the ap id and name based on the client's mac %s.", mac)
    for connection in ssh:
        logger.info("Come here.")
    stdin, stdout, stderr = ssh[0].exec_command("get ap status")
    stdin.write('lol\n')
    stdin.flush()
    print(stdout.readlines())
    print(stderr.readlines())
    # print(stdout)
    for line in stdout:  
        splitted = line.split(" ")
        print splitted

    for line in stderr:  
        splitted = line.split(" ")
        print splitted
        #print splitted
        # if mac == splitted[0]:
        #     # 8 is the AP ID, and 12 is the AP name
        #     logger.info("Exit: Getting the ap id and name based on the client's mac %s.", mac)
        #     return splitted[8], splitted[12]
    # TODO: TEST THIS ERROR.
    logger.error("Exit: Getting the ap id and name based on the mac %s - did not find ap id and name.", mac)
    return "", ""


# Get the ap mac adres and name based on the ap id and name
def get_ap_mac(ssh, apid, apname):
    logger.info("Enter: Getting the ap mac based on ap id %s and name %s.", apid, apname)
    sstdin, stdout, stderr = ssh.exec_command("show ap")
    for line in stdout:  
        splitted = line.split(" ")
        if apid == splitted[0] and apname == splitted[5]:
            # 21 is the AP's 'serial' nr. aka MAC.
            logger.info("Exit: Getting the ap mac (%s) based on ap id %s and name %s.", splitted[21], apid, apname)
            return splitted[21]
    # TODO: TEST THIS ERROR.
    logger.error("Exit: Getting the ap mac - did not find ap mac (apid %s and apname %s).", apid, apname)
    return "", ""

def fwd_location(client_id):
    logger.info("Enter: forward location call of client id %s.", client_id)
    found = False
    msg = ""
    # Try to set up the connnection to the controller.
    ssh = setup_ssh_connection()

    if ssh is not None:
        # Fetch the ap data.
        apid, apname = get_ap_id_name(ssh, client_id)
        if apid != "" and apname != "":
            apmac = get_ap_mac(ssh, apid, apname)
            if apmac != "":
                # try:
                found = True
                location[client_id] = apmac
                location_attempts[client_id] = -1
                #### Comment here.
                # location_transmits[client_id] = settings.wb["location_interval_transmits"]
                # logger.info("Immediately found the location (%s) of client id %s (attempts initialized to %s, transmits to %s)", apmac, client_id, location_attempts[client_id], location_transmits[client_id])
                #### Comment here - end.
                #### Uncomment below.
                logger.info("Immediately found the location (%s) of client id %s (attempts initialized to %s)", apmac, client_id, location_attempts[client_id])
            else:
                logger.error("Forward location call - could not find ap mac associated to ap id %s and name %s.", apid, apname)
        else:
            logger.error("Forward location call - could not find AP's ap id and name based on the client id %s.", client_id)
        

        if found == False:
            logger.info("Forward location call - set station's location to default value.")
            location[client_id] = settings.wb["unknown_mac_location"]
            location_attempts[client_id] = -1
            #### Comment here.
            # location_transmits[client_id] = settings.wb["location_interval_transmits"]
            #### Comment here - end.

        clientLogger.info("Location of new client: " + str(client_id) + ", AP = " + str(location[client_id]) + ", attempts = " + str(location_attempts[client_id]))

        try:
            addr = settings.ip["ugc"] + "/v1/location"
            # The AP's id in the SELVIE documentation is actually its MAC address.
            payload = {'client_id': client_id, 'ap_id': location[client_id]}
            headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
            r = requests.post(addr, data=json.dumps(payload), headers=headers, timeout=settings.wb["max_request_timeout"])
            json_response = r.json()
            if (r.status_code != 200):
                raise Exception("Status code of call is NOT 200, but " + str(r.status_code))
            if ('status' not in json_response or 'response' not in json_response or type(json_response["response"]) is not dict):
                raise Exception("Response of forward location RESTFUL had the wrong syntax: " + str(json_response))
            if (json_response["status"] != 'OK'):
                raise Exception("Response of confirm request RESTFUL contained a status NOT OK: " + str(json_response))

            # Everything went well.
            logger.info("Exit: forward location call of client id %s.", client_id)
        except Exception as detail:
            logger.exception("Exit: forward location call - could not forward location (ap mac %s) of client id %s to UGC (%s): %s.", location[client_id], client_id, settings.ip["ugc"], detail)
    else:
        logger.error("Exit: forward location call of client id %s - could not set up ssh connection.", client_id)


# Check periodically the locations of all the connected clients.
def fetch_locations(ssh):
    logger.info("Enter: Fetch each client's location.")

    if len(socket_clienthandlers) > 0:
        # Try to set up the connnection to the controller.
        #ssh = setup_ssh_connection()
        if ssh is not None:
            # Get the necessary information.
            stdin, stdout, stderr = ssh.exec_command("show station")
            lines = stdout.readlines()
            stdin_ap, stdout_ap, stderr_ap = ssh.exec_command("show ap")
            lines_ap = stdout_ap.readlines()

            # Update all the information.
            for client_id, val in socket_clienthandlers.iteritems():
                # If the client_id is not yet in the location attempts, initiliaze it.
                if (client_id not in location_attempts.keys()):
                    location_attempts[client_id] = 0
                # here should also come something for the location transmits.
                found = False
                for line in lines:
                    splitted = line.split(" ")
                    # If the station is found in the output of the show station command.
                    if client_id == splitted[0]:
                        for line_ap in lines_ap:
                            splitted_ap = line_ap.split(" ")
                            # If the ap id and ap name are found in the output of the "show ap" command.
                            if splitted_ap[0] == splitted[8] and splitted_ap[5] == splitted[12]:
                                # Save the location.
                                #logger.info("what %s",splitted_ap[21])
                                #logger.info("what %s", location[client_id])
                                if client_id in location.keys() and location[client_id] == splitted_ap[21]:
                                    # If the location is still the same, make this clear by setting to -1.
                                    # This way, the update knows no update should be sent.
                                    location_attempts[client_id] = -1
                                    logger.info("Found identical location %s for client_id %s (attempts set to %s).", splitted_ap[21], client_id, location_attempts[client_id])
                                else:
                                    # If the situation changed, make this clear by setting to 0.
                                    location[client_id] = splitted_ap[21]
                                    location_attempts[client_id] = 0
                                    #### Comment here.
                                    # location_transmits[client_id] = settings.wb["location_interval_transmits"]
                                    #### Comment here - end.
                                    logger.info("Found new location %s for client_id %s (attempts reset to %s)", splitted_ap[21], client_id, location_attempts[client_id])
                                found = True
                # If the station or AP is not found at all, reset the location and add an attempt to the counter.
                if not found:
                    if location[client_id] == settings.wb["unknown_mac_location"]:
                        location_attempts[client_id] = location_attempts[client_id] + 1
                        #### Comment here.
                        # if location_attempts[client_id] > settings.wb["max_location_attempts"]:
                        #     del socket_clienthandlers[client_id]
                        #     if client_id in location.keys():
                        #         del location[client_id]
                        #     if client_id in location_attempts.keys():
                        #         del location_attempts[client_id]
                        #     if client_id in location_transmits.keys():
                        #         del location_transmits[client_id]
                        #     logger.info("Cleaned client %s in fetch locations.", client_id)
                        #### Comment here - end.
                    else:
                        location[client_id] = settings.wb["unknown_mac_location"]
                        location_attempts[client_id] = 0
                        #### Comment here.
                        # location_transmits[client_id] = settings.wb["location_interval_transmits"]
                        #### Comment here - end.
                    logger.info("Did not found location for client id %s (attempts: %s).", client_id, location_attempts[client_id])
            logger.info("Exit: Fetch each client's location.")
        else:
            logger.error("Exit: Fetch each client's location - could not set up ssh connection.")
    else:
        logger.info("Exit: Fetch each client's location (there were no clients).")

# def cleanup_clients():
#     logger.info("Enter: clean all clients who are not reachable anymore.")
#     # Using items(), you can delete while iterating it.
#     # !!! WILL ONLY WORK IN PYTHON < 3.0 !!!
#     count = 0
#     for client_id, attempts in location_attempts.items():
#         if attempts > 3:
#             count = count + 1
#             # TODO: close connection
#             # del socket_clienthandlers[client_id]
#             del location[client_id]
#             del location_attempts[client_id]
#     logger.info("Exit: clean all clients who are not reachable anymore (%s cleaned).", count)

def send_location_updates():
    logger.info("Enter: Send location updates to the UGC.")
    for client_id, ap_mac in location.items():
        try:
            # If it is equal to 0, there is a new location.
            # -1 signal it is still at the same location => no update needed.
            # > 0, the location was not found.
            if location_attempts[client_id] == 0:
            #if location_attempts[client_id] == 0 or location_transmits[client_id] > 0:
                # The AP's id in the SELVIE documentation is actually its MAC address.
                addr = settings.ip["ugc"] + "/v1/location/" + client_id
                # The AP's id in the SELVIE documentation is actually its MAC address.
                payload = {'client_id': client_id, 'ap_id': ap_mac}
                headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
                r = requests.put(addr, data=json.dumps(payload), headers=headers, timeout=settings.wb["max_request_timeout"])
                json_response = r.json()
                if (r.status_code != 200):
                    raise Exception("Status code of call is NOT 200, but " + str(r.status_code))
                if ('status' not in json_response or 'response' not in json_response or type(json_response["response"]) is not dict):
                   raise Exception("Response of forward location RESTFUL had the wrong syntax: " + str(json_response))
                if (json_response["status"] != 'OK'):
                   raise Exception("Response of confirm request RESTFUL contained a status NOT OK: " + str(json_response))
                #### Comment here.
                # if location_transmits[client_id] > 0:
                #     location_transmits[client_id] = location_transmits[client_id] - 1
                #     logger.info("Decremented the transmits of client_id %s to %s.", client_id, location_transmits[client_id])
                #### Comment here - end.
                logger.info("Exit: Send location updates to the UGC.")
        except Exception as detail:
            logger.exception("Exit: send location updates - could not send location updates (ap mac %s) of client id %s to UGC (%s): %s.", ap_mac, client_id, settings.ip["ugc"], detail)
    if (len(location) == 0):
        logger.info("Exit: Send location updates to the UGC cause of no clients.")

def update_locations(ssh):
    logger.info("Enter: update locations of all clients [PERIODICALLY]")
    # First, fetch the locations of all known clients.
    fetch_locations(ssh)
    # This function will check if the attempts of getting certain clients exceed the limit. If so, close their connection.
    # cleanup_clients()
    # Now, send the updates to the UGC.
    send_location_updates()
    log_clients_locations_overview()
    logger.info("Exit: update locations of all clients [PERIODICALLY]")

################ TO UE ##################

def fwd_content_request(payload):
    logger.info("Enter: (to UE) forward content request call for client_id %s.", payload["client_id"])
    # add sendStartTime
    # add sendRate
    # del deadlinePolicy
    # del deadline
    sockethandler = socket_clienthandlers.get(payload["client_id"])
    payload["message"] = "content_request"
    del payload["deadlinePolicy"]
    del payload["deadline"]
    payload["SendStartTime"] = "0"
    payload["sendRate"] = "54Mbps"
    strpayload = json.dumps(payload).encode('utf8')
    sockethandler.sendClientMessage(strpayload, payload["client_id"])
    logger.info("Exit: (to UE) forward content request call for client_id %s.", payload["client_id"])


def fwd_content_cancel(payload):
    logger.info("Enter: (to UE) forward content request call for client_id %s.", payload["client_id"])
    sockethandler = socket_clienthandlers.get(payload["client_id"])
    payload["message"] = "content_cancel"
    strpayload = json.dumps(payload).encode('utf8')
    sockethandler.sendClientMessage(strpayload, payload["client_id"])
    logger.info("Exit: (to UE) forward content request callfor client_id %s.", payload["client_id"])


def fwd_device_reconfig(payload, client_id):
    logger.info("Enter: (to UE) forward device reconfiguration call for client_id %s.", client_id)
    sockethandler = socket_clienthandlers.get(client_id)
    payload["message"] = "device_reconfig"
    strpayload = json.dumps(payload).encode('utf8')
    sockethandler.sendClientMessage(strpayload, client_id)
    logger.info("Exit: (to UE) forward device reconfiguration call for client_id %s.", client_id)