# SELVIE project
# Wireless Broker implementation
# @author Glenn Daneels

from flask import Flask, request, abort, jsonify
from flask.ext.restful import Resource, Api
from SimpleWebSocketServer import WebSocket, SimpleWebSocketServer
from WBWebSocket import fwd_content_request, fwd_content_cancel, fwd_device_reconfig, clienthandler_exist
import logging
import random
import thread
import json
import paramiko
import requests
import settings

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("WirelessBroker")
clientLogger = logging.getLogger("ClientOverview")

# Default Flask stuff.
app = Flask(__name__)
# Default Flask Restful stuff.
api = Api(app)

def make_error(status, message, fName, payload="none"):
    response = jsonify({
        'status': status,
        'message': message
    })
    response.status = status
    logger.error("In function %s: message \"%s\" and status %s (payload %s).", fName, message, status, payload)
    return response, 200

################ FROM UGC ##################

# Succesfull query
# curl -H "Content-Type: application/json" http://localhost:8001/v1/content/request -X POST -d '{"request_id": "1", "client_id": "7a:31:c1:2f:5b:00", "content_id": "3", "contentStartTime": 1429521187492, "contentEndTime": 1429521187492, "deadlinePolicy": "deadlinePolicy", "deadline": "deadline"}' -v
# curl -H "Content-Type: application/json" http://10.10.131.194:5000/v1/content/request -X POST -d '{"request_id": 1, "client_id": "f8:a9:d0:8a:a8:8c", "content_id": 3, "contentStartTime": "contentStartTime", "contentEndTime": "contentEndTime", "deadlinePolicy": "deadlinePolicy", "deadline": "deadline"}' -v
# Faulty JSON data: curl -H "Content-Type: application/json" http://localhost:5000/v1/content/request -X POST -d '{"request_id": 1, "client_id": "7a:31:c1:2f:5b:00", "content_id": 3, "contentStartTime": "contentStartTime", "contentEndTime": "contentEndTime" "deadlinePolicy": "deadlinePolicy", "deadline": "deadline"}' -v
@app.route('/v1/content/request', methods=['POST'])
def content_request():
    logger.info("Enter: (from UGC) content request call.")
    if not request.json:
        return make_error('400', 'no json', 'content_request')
    if 'request_id' not in request.json or type(request.json['request_id']) is not unicode:
        return make_error('400', 'request_id fault', 'content_request', request.json)
    if 'client_id' not in request.json or type(request.json['client_id']) is not unicode:
        return make_error('400', 'client_id fault', 'content_request', request.json)
    if 'content_id' not in request.json or type(request.json['content_id']) is not unicode:
        return make_error('400', 'content_id fault', 'content_request', request.json)
    if 'contentStartTime' not in request.json or type(request.json['contentStartTime']) is not int:
        return make_error('400', 'contentStartTime fault', 'content_request', request.json)
    if 'contentEndTime' not in request.json or type(request.json['contentEndTime']) is not int:
    	return make_error('400', 'contentEndTime fault', 'content_request', request.json)
    if 'deadlinePolicy' not in request.json or type(request.json['deadlinePolicy']) is not unicode:
    	return make_error('400', 'deadlinePolicy fault', 'content_request', request.json)
    if 'deadline' not in request.json or type(request.json['deadline']) is not unicode:
    	return make_error('400', 'deadline fault', 'content_request', request.json)

    logger.info("(from UGC) CONTENT REQUEST call for client_id %s, request_id %s and content_id %s.", request.json['client_id'], request.json['request_id'], request.json['content_id'])

    if (clienthandler_exist(request.json['client_id'])):
        fwd_content_request(request.json)
    else:
        return make_error('400', 'no socket connection', 'content_request', request.json)
    
    logger.info("Exit: (from UGC) content request call.")
    return jsonify({'status': "OK", 'estimatedArrivalTime': 1}), 200

# Succesfull query
# curl -H "Content-Type: application/json" http://localhost:5000/v1/content/request/1 -X DELETE -d '{"client_id": "7a:31:c1:2f:5b:00", "request_id": "1"}' -v
# curl -H "Content-Type: application/json" http://192.168.1.129:5000/v1/content/request/1 -X DELETE -d '{"client_id": "30:75:12:b5:3c:c5", "request_id": "1"}' -v
@app.route('/v1/content/request/<int:request_id>', methods=['DELETE'])
def content_cancel(request_id):
    logger.info("Enter: (from UGC) content cancel call.")
    if not request.json:
        return make_error('400', 'no json', 'content_cancel')
    if 'client_id' not in request.json or type(request.json['client_id']) is not unicode:
        return make_error('400', 'client_id', 'content_cancel', request.json)
    if 'request_id' not in request.json or type(request.json['request_id']) is not unicode:
        return make_error('400', 'request_id', 'content_cancel', request.json)

    logger.info("(from UGC) CONTENT CANCEL call for client_id %s and request_id %s.", request.json['client_id'], request.json['request_id'])

    if (clienthandler_exist(request.json['client_id'])):
        fwd_content_cancel(request.json)
    else:
        return make_error('400', 'no socket connection', 'content_cancel', request.json)

    logger.info("Exit: (from UGC) content cancel call.")
    return jsonify({'status': 'OK'}), 200


# Succesfull query
# curl -H "Content-Type: application/json" http://127.0.0.1:8001/v1/configuration/7a:31:c1:2f:5c:00 -X POST -d '{"commands": [{"resolution":"800x600"}]}' -v
@app.route('/v1/configuration/<string:client_id>', methods=['POST'])
def device_reconfiguration(client_id):
    logger.info("Enter: (from UGC) device reconfiguration call.")
    if not request.json:
        return make_error('400', 'no json', 'device_reconfiguration', request.json)
    if 'commands' not in request.json or type(request.json['commands']) is not list:
        return make_error('400', 'commands fault', 'device_reconfiguration', request.json)

    logger.info("(from UGC) DEVICE RECONFIGURATION call for client_id %s.", client_id)

    if (clienthandler_exist(client_id)):
        fwd_device_reconfig(request.json, client_id)
    else:
        return make_error('400', 'no socket connection', 'device_reconfiguration', request.json)

    logger.info("Exit: (from UGC) device reconfiguration call.")
    return jsonify({'status': 'OK'}), 200

################ FROM UE ##################

# Succesfull query
# curl -H "Content-Type: application/json" http://127.0.0.1:8001/v1/device/parameter/7a:31:c1:2f:5c:00 -X PUT -d '{"name": "goodput", "value": "54.0", "unit": "Mbps", "aggregationType": "max", "aggregationRangeStart": "datetime", "aggregationRangeEnd": "datetime"}' -v
@app.route('/v1/device/parameter/<string:client_id>', methods=['PUT'])
def device_parameter(client_id):
    logger.info("Enter: (from UE %s) device parameter update call.", client_id)
    if not request.json:
        return make_error('400', 'no json', 'device_parameter', request.json)
    if 'name' not in request.json or type(request.json['name']) is not unicode:
        return make_error('400', 'name fault', 'device_parameter', request.json)
    if 'value' not in request.json or type(request.json['value']) is not unicode:
        return make_error('400', 'value fault', 'device_parameter', request.json)
    if 'unit' not in request.json or type(request.json['unit']) is not unicode:
        return make_error('400', 'unit fault', 'device_parameter', request.json)
    if 'aggregationType' not in request.json or type(request.json['aggregationType']) is not unicode:
        return make_error('400', 'aggregationType fault', 'device_parameter', request.json)
    if 'aggregationRangeStart' not in request.json or type(request.json['aggregationRangeStart']) is not unicode:
        return make_error('400', 'aggregationRangeStart fault', 'device_parameter', request.json)
    if 'aggregationRangeEnd' not in request.json or type(request.json['aggregationRangeEnd']) is not unicode:
        return make_error('400', 'aggregationRangeEnd fault', 'device_parameter', request.json)

    logger.info("(from UE) DEVICE PARAMETER call for client_id %s.", client_id)

    logger.info("Exit: (from UE %s)  device parameter update call.", client_id)
    return jsonify({'status': 'OK'}), 200

###### BEGIN - These are dummy interfaces to test the functionality of the WB ######

# @app.route('/v1/location', methods=['POST'])
# def location_post():
#     logger.info("Enter: location post call.")

#     if not request.json:
#         return make_error('400', 'no json', 'location_update')
#     if 'client_id' not in request.json or type(request.json['client_id']) is not unicode:
#         return make_error('400', 'client_id fault', 'location_update', request.json)
#     if 'ap_id' not in request.json or type(request.json['ap_id']) is not unicode:
#         return make_error('400', 'ap_id fault', 'location_update', request.json)

#     logger.info("Location post call for client_id %s and ap_id %s.", request.json['client_id'], request.json['ap_id'])

#     logger.info("Exit: location post call for client_id %s.", request.json['client_id'])
#     return jsonify({'status': 'OK', 'response': {}}), 200

# @app.route('/v1/location/<string:client_id>', methods=['PUT'])
# def location_update(client_id):
#     logger.info("Enter: location update call for client_id %s.", client_id)
#     if not request.json:
#         return make_error('400', 'no json', 'location_update')
#     if 'client_id' not in request.json or type(request.json['client_id']) is not unicode:
#         return make_error('400', 'client_id fault', 'location_update', request.json)
#     if 'ap_id' not in request.json or type(request.json['ap_id']) is not unicode:
#         return make_error('400', 'ap_id fault', 'location_update', request.json)

#     logger.info("Location post call for client_id %s and ap_id %s.", request.json['client_id'], request.json['ap_id'])

#     logger.info("Exit: location update for client_id %s.", request.json['client_id'])
#     return jsonify({'status': 'OK', 'response': {}}), 200

@app.route('/v1/location', methods=['PUT'])
def location_update():
    logger.info("Enter: location update call.")
    if not request.json:
        return make_error('400', 'no json', 'location_update')
    # logger.info("object %s", type(request.json['locations']))
    logger.info("Locations list %s", request.json['locations'])
    if 'locations' not in request.json or type(request.json['locations']) is not unicode:
        return make_error('400', 'locations fault', 'location_update', request.json)

    logger.info("Locations update call.")

    logger.info("Exit: location update.")
    return jsonify({'status': 'OK', 'response': {}}), 200

@app.route('/v1/request/confirmation', methods=['POST'])
def content_request_confirmation():
    logger.info("Enter: content confirmation call.")
    if not request.json:
        return make_error('400', 'no json', 'content_request_confirmation')
    if 'request_id' not in request.json or type(request.json['request_id']) is not unicode:
        return make_error('400', 'request_id fault', 'content_request_confirmation', request.json)
    if 'content_id' not in request.json or type(request.json['content_id']) is not unicode:
        return make_error('400', 'content_id fault', 'content_request_confirmation', request.json)

    logger.info("Content request confirmation call for request_id %s and content_id %s.", request.json['request_id'], request.json['content_id'])

    logger.info("Exit: content confirmation call for request id %s and content_id %s.", request.json['request_id'], request.json['content_id'])
    return jsonify({'status': 'OK', 'response': {}}), 200

@app.route('/v1/request/cancel', methods=['POST'])
def content_cancel_confirmation():
    logger.info("Enter: content cancel confirmation call.")
    if not request.json:
        return make_error('400', 'no json', 'content_cancel_confirmation')
    if 'request_id' not in request.json or type(request.json['request_id']) is not unicode:
        return make_error('400', 'request_id fault', 'content_cancel_confirmation', request.json)
    if 'content_id' not in request.json or type(request.json['content_id']) is not unicode:
        return make_error('400', 'content_id fault', 'content_cancel_confirmation', request.json)

    logger.info("Content cancel request confirmation call for request_id %s and content_id %s.", request.json['request_id'], request.json['content_id'])

    logger.info("Exit: content cancel confirmation call for request id %s and content_id %s.", request.json['request_id'], request.json['content_id'])
    return jsonify({'status': 'OK', 'response': {}}), 200


###### END - These are dummy interfaces to test the functionality of the WB ######

###### BEGIN - GUI #######
# @app.route('/gui')
# def show_gui():
#     output = ''
#     output += show_connected_clients()
#     return output

# def show_connected_clients():
#     nr_clients = len(socket_clienthandlers)
#     output = '<table border="1"><tr><td></td><th>Connected clients (' + str(nr_clients) + ')</th><th>Location</th><th>Location attempts</th></tr>'
#     if nr_clients > 0:
#         count = 0
#         for client in socket_clienthandlers:
#             count = count + 1
#             output += '<tr><td>' + str(count) + '</td><td>' + str(client) + '</td><td>' + str(location[client]) + '</td><td>' + str(location_attempts[client]) + '</td></tr>'
#     else:
#         output += '<tr><td>/</td><td><i>No clients connected</i></td><td>/</td><td>/</td></tr>'
#     output += '<table>'
#     return output

###### END - GUI #######