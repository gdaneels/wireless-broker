import thread
import time
import logging
import os
import datetime
import settings
import paramiko
import StringIO

logging.basicConfig(level=logging.DEBUG)

# Run this only if the script is ran directly.
if __name__ == '__main__':
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect('192.168.1.147', username='config', password='config')


    chan = ssh.get_transport().open_session()

    chan.settimeout(1)

    try:
        # Execute the given command
        chan.exec_command('help')

        # To capture Data. Need to read the entire buffer to caputure output

        contents = StringIO.StringIO()
        error = StringIO.StringIO()

        chan.recv_exit_status()

        while not chan.recv_exit_status():
            if chan.recv_ready():
                    data = chan.recv(1024)
                    #print "Indside stdout"
                    while data:
                        contents.write(data)
                        data = chan.recv(1024)
            if chan.recv_stderr_ready():
                    error_buff = chan.recv_stderr(1024)
                    while error_buff:
                        error.write(error_buff)
                        error_buff = chan.recv_stderr(1024)
        
        exist_status = chan.recv_exit_status()


    except socket.timeout:
      raise socket.timeout

    output = contents.getvalue()
    error_value = error.getvalue()

    print output,error_value,exist_status