import thread
import time
import paramiko
import logging
import os
parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.sys.path.insert(0,parentdir) 
import datetime
import sys
import settings

logging.basicConfig(level=logging.INFO)

logger = logging.getLogger("AllLogger")
logger.propagate = False

aplogger = logging.getLogger("ApLogger")
aplogger.propagate = False

i1logger = logging.getLogger("InterfaceSpecificLogger")
i1logger.propagate = False

i2logger = logging.getLogger("InterfaceLogger")
i2logger.propagate = False

slogger = logging.getLogger("StationLogger")
slogger.propagate = False

s1logger = logging.getLogger("Station1Logger")
s1logger.propagate = False

s2logger = logging.getLogger("Station2Logger")
s2logger.propagate = False

s3logger = logging.getLogger("Station3Logger")
s3logger.propagate = False

s4logger = logging.getLogger("Station4Logger")
s4logger.propagate = False

formatter = logging.Formatter('%(asctime)s - %(name)s - %(module)s - %(levelname)s - %(message)s')

def monitor_show_ap():
    ssh = setup_ssh_connection()
    try:
        while True:
            a = datetime.datetime.now()
            aps = list(set([x[0] for x in settings.stats["aps_interfaces"]]))
            for apIndex in aps:
                command = "show ap " + str(apIndex)
                stdin, stdout, stderr = ssh.exec_command(command)
                aplogger.info(command)
                aplogger.info(stdout.read())
            
            b = datetime.datetime.now()
            elapsed = (b - a)
            if (elapsed < datetime.timedelta(seconds=settings.stats["interval"])):
                time.sleep(settings.stats["interval"]-elapsed.total_seconds())
    except KeyboardInterrupt, Exception:
        ssh.close()
        logger.info("Closed ssh connection in monitor_show_ap.")

def monitor_show_interfaces():
    ssh = setup_ssh_connection()
    try:
        while True:
            a = datetime.datetime.now()
            
            stdin, stdout, stderr = ssh.exec_command("show interfaces Dot11Radio statistics")
            i1logger.info(stdout.read())
            
            b = datetime.datetime.now()
            elapsed = (b - a)
            if (elapsed < datetime.timedelta(seconds=settings.stats["interval"])):
                time.sleep(settings.stats["interval"]-elapsed.total_seconds())
    except KeyboardInterrupt, Exception:
        ssh.close()
        logger.info("Closed ssh connection in monitor_show_interfaces.")

def monitor_show_interfaces_ap_iface():
    ssh = setup_ssh_connection()
    try:
        while True:
            a = datetime.datetime.now()
            
            for ap_iface_tuple in settings.stats["aps_interfaces"]:
                command = "show interfaces Dot11Radio statistics " + str(ap_iface_tuple[0]) + " " + str(ap_iface_tuple[1])
                stdin, stdout, stderr = ssh.exec_command(command)
                i2logger.info(command)
                i2logger.info(stdout.read())
            
            b = datetime.datetime.now()
            elapsed = (b - a)
            if (elapsed < datetime.timedelta(seconds=settings.stats["interval"])):
                time.sleep(settings.stats["interval"]-elapsed.total_seconds())
    except KeyboardInterrupt, Exception:
        ssh.close()
        logger.info("Closed ssh connection in monitor_show_interfaces_ap_iface.")

def monitor_show_stations_per_ap():
    ssh = setup_ssh_connection()
    try:
        while True:
            a = datetime.datetime.now()
            
            stdin, stdout, stderr = ssh.exec_command("show statistics station-per-ap")
            slogger.info(stdout.read())
            
            b = datetime.datetime.now()
            elapsed = (b - a)
            if (elapsed < datetime.timedelta(seconds=settings.stats["interval"])):
                time.sleep(settings.stats["interval"]-elapsed.total_seconds())
    except KeyboardInterrupt, Exception:
        ssh.close()
        logger.info("Closed ssh connection in monitor_show_stations_per_ap.")

def monitor_show_station():
    ssh = setup_ssh_connection()
    try:
        while True:
            a = datetime.datetime.now()
            
            stdin, stdout, stderr = ssh.exec_command("show station")
            s1logger.info(stdout.read())
            
            b = datetime.datetime.now()
            elapsed = (b - a)
            if (elapsed < datetime.timedelta(seconds=settings.stats["interval"])):
                time.sleep(settings.stats["interval"]-elapsed.total_seconds())
    except KeyboardInterrupt, Exception:
        ssh.close()
        logger.info("Closed ssh connection in monitor_show_station.")

def monitor_show_station_counter():
    ssh = setup_ssh_connection()
    try:
        while True:
            a = datetime.datetime.now()
            
            stdin, stdout, stderr = ssh.exec_command("show station counter")
            s2logger.info(stdout.read())
            
            b = datetime.datetime.now()
            elapsed = (b - a)
            if (elapsed < datetime.timedelta(seconds=settings.stats["interval"])):
                time.sleep(settings.stats["interval"]-elapsed.total_seconds())
    except KeyboardInterrupt, Exception:
        ssh.close()
        logger.info("Closed ssh connection in monitor_show_station_counter.")

def monitor_show_station_80211():
    ssh = setup_ssh_connection()
    try:
        while True:
            a = datetime.datetime.now()
            
            stdin, stdout, stderr = ssh.exec_command("show station 802.11")
            s3logger.info(stdout.read())
            
            b = datetime.datetime.now()
            elapsed = (b - a)
            if (elapsed < datetime.timedelta(seconds=settings.stats["interval"])):
                time.sleep(settings.stats["interval"]-elapsed.total_seconds())
    except KeyboardInterrupt, Exception:
        ssh.close()
        logger.info("Closed ssh connection in monitor_show_station_80211.")


def monitor_station_per_ap_specific():
    ssh = setup_ssh_connection()
    try:
        while True:
            macs = {}
            a = datetime.datetime.now()
            
            stdin, stdout, stderr = ssh.exec_command("show station")
            lines = stdout.readlines()
            for line in lines:
                splitted = line.split(" ")
                if ":" in splitted[0]:
                    macs[splitted[0]] = splitted[8]
                    # Then, it is a MAC address.

            for macaddress, apid in macs.iteritems():
                command = "show statistics station-per-ap " + str(apid) + " " + str(macaddress)
                s4logger.info(command)
                stdin1, stdout1, stderr1 = ssh.exec_command(command)
                s4logger.info(stdout1.read())
            
            b = datetime.datetime.now()
            elapsed = (b - a)
            if (elapsed < datetime.timedelta(seconds=settings.stats["interval"])):
                time.sleep(settings.stats["interval"]-elapsed.total_seconds())
    except KeyboardInterrupt, Exception:
        ssh.close()
        logger.info("Closed ssh connection in monitor_station_per_ap_specific.")

def setup_ssh_connection():
    logger.info("Enter: Setting up the ssh connection (user = %s, passw = %s, address = %s).", settings.controller["username"], settings.controller["password"], settings.ip["controller"])
    # Set up the ssh call.
    try:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(settings.ip["controller"], username=settings.controller["username"], password=settings.controller["password"])
        logger.info("Exit: Setting up the ssh connection (user = %s, passw = %s, address = %s).", settings.controller["username"], settings.controller["password"], settings.ip["controller"])
        return ssh
    except:
        logger.error("Exit: could not set up ssh connection (user = %s, passw = %s, address = %s).", settings.controller["username"], settings.controller["password"], settings.ip["controller"])
        return None
    logger.error("Exit: could not set up ssh connection (user = %s, passw = %s, address = %s).", settings.controller["username"], settings.controller["password"], settings.ip["controller"])
    return None

def test_connections():
    logger.info("Enter: Testing external connections.") 
    try:
        ssh = setup_ssh_connection()
        if (ssh != None):
            logger.info("SSH connection successfully tested.")
            logger.info("Exit: Testing external connections.")
            ssh.close()
        else:
            raise Exception("Not able to set up SSH connection.")
    except Exception as detail:
        logger.error("Exit: Testing external connections - Exception: %s", detail)

# Run this only if the script is ran directly.
if __name__ == '__main__':
    datetimestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H-%M-%S')
    experiment_directory = settings.stats["experiment_directory"] + "/experiment-" + str(datetimestamp)
    if not os.path.exists(experiment_directory):
        os.makedirs(experiment_directory)

    all_log = experiment_directory + "/all.log"
    show_ap_log = experiment_directory + "/show-ap.log"
    show_interfaces_log = experiment_directory + "/show-interfaces.log"
    show_interfaces_ap_iface_log = experiment_directory + "/show-interfaces-ap-iface.log"
    show_station_per_ap_log = experiment_directory + "/show-station-per-ap.log"
    show_station = experiment_directory + "/show-station.log"
    show_station_counter_log = experiment_directory + "/show-station-counter.log"
    show_station_80211_log = experiment_directory + "/show-station-80211.log"
    show_station_per_ap_specific_log = experiment_directory + "/show-station-per-ap-specific.log"

    # create file handler which logs even debug messages
    fh = logging.FileHandler(all_log)
    fh.setLevel(logging.INFO)
    fh.setFormatter(formatter)
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    ch.setFormatter(formatter)
    logger.addHandler(fh)
    logger.addHandler(ch)

    # create file handler which logs even debug messages
    fh = logging.FileHandler(show_ap_log)
    fh.setLevel(logging.INFO)
    fh.setFormatter(formatter)
    aplogger.addHandler(fh)

    fh = logging.FileHandler(show_interfaces_log)
    fh.setLevel(logging.INFO)
    fh.setFormatter(formatter)
    i1logger.addHandler(fh)

    fh = logging.FileHandler(show_interfaces_ap_iface_log)
    fh.setLevel(logging.INFO)
    fh.setFormatter(formatter)
    i2logger.addHandler(fh)

    # create file handler which logs even debug messages
    fh = logging.FileHandler(show_station_per_ap_log)
    fh.setLevel(logging.INFO)
    fh.setFormatter(formatter)
    slogger.addHandler(fh)

    fh = logging.FileHandler(show_station)
    fh.setLevel(logging.INFO)
    fh.setFormatter(formatter)
    s1logger.addHandler(fh)

    fh = logging.FileHandler(show_station_counter_log)
    fh.setLevel(logging.INFO)
    fh.setFormatter(formatter)
    s2logger.addHandler(fh)

    fh = logging.FileHandler(show_station_80211_log)
    fh.setLevel(logging.INFO)
    fh.setFormatter(formatter)
    s3logger.addHandler(fh)

    fh = logging.FileHandler(show_station_per_ap_specific_log)
    fh.setLevel(logging.INFO)
    fh.setFormatter(formatter)
    s4logger.addHandler(fh)

    test_connections()

    thread.start_new_thread(monitor_show_interfaces, ())
    thread.start_new_thread(monitor_show_interfaces_ap_iface, ())
    thread.start_new_thread(monitor_show_stations_per_ap, ())
    thread.start_new_thread(monitor_show_ap, ())
    thread.start_new_thread(monitor_show_station, ())
    thread.start_new_thread(monitor_show_station_counter, ())
    thread.start_new_thread(monitor_show_station_80211, ())
    thread.start_new_thread(monitor_station_per_ap_specific, ())
    while True:
        True