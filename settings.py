# # Configuration Wireless broker and monitoring.

# public = True

# # something else from 0 is Meru.
# # ap = 0 # Meru
# # ap = 1 # AirTight

# airtight = False # Indicates you are using the AirTight AP.

# ip = {"wb": "192.168.0.3", "ugc": "http://192.168.0.55:6666", "director": "http://192.168.0.51:3000", "controller": "192.168.0.5"}
# controller = {"username": "admin", "password": "matteoglenn"}
# wb = {"port_restful": 8001, "port_websockets": 5000, "location_update_interval": 15, "location_interval_transmits": 2, "max_location_attempts": 5, "max_request_timeout": 1.0, "unknown_mac_location": "00:00:00:00:00:00", "dummy_location": "00:00:00:00:00:01", "experiment_directory": "experiments"}
# stats = {"interval": 5, "experiment_directory": "experiments", "aps_interfaces": [(1,1), (1,2), (3,1), (3,2), (2,1)]}

# # AirTight related stuff.
# aps = ["192.168.1.147"]
# cloudcontroller = {"username": "config", "password": "config"}

# Configuration Wireless broker and monitoring.

public = False

# something else from 0 is Meru.
# ap = 0 # Meru
# ap = 1 # AirTight

airtight = False # Indicates you are using the AirTight AP.

ip = {	
		"wb": "127.0.0.1", 
		"ugc": "http://127.0.0.1:8001", 
		"director": "http://127.0.0.1:3000", 
		"controller": "192.168.0.5"
	}

controller = {
		"username": "admin", 
		"password": "matteoglenn"
	}

wb = {	
		"port_restful": 8001, 
		"port_websockets": 5000,
		"location_update_interval": 15,
		"location_interval_transmits": 2,
		"max_location_attempts": 2,
		"max_ssh_timeout": 10,
		"max_request_timeout": 1.0,
		"unknown_mac_location": "00:00:00:00:00:00",
		"dummy_location": "00:00:00:00:00:01",
		"experiment_directory": "experiments"
		}

stats = {
		"interval": 5, 
		"experiment_directory": "experiments",
		"aps_interfaces": [(1,1), (1,2), (3,1), (3,2), (2,1)]
		}

# AirTight related stuff.
aps = ["192.168.1.147"]
cloudcontroller = {"username": "config", "password": "config"}