# SELVIE project
# Wireless Broker WebSocket server implementation.
# This module contains all methods related to the Wireless Broker Websocket server.
# @author Glenn Daneels

import requests
import json
import logging
import thread
import settings
from SimpleWebSocketServer import WebSocket, SimpleWebSocketServer
from WBController import location, location_attempts
from flask import jsonify

# The mac addresses of the clients mapped to their WebSocket handlers.
socket_clienthandlers = {}

# Set the logging level.
logging.basicConfig(level=logging.DEBUG)
# Get the generic Wireless Broker logger.
logger = logging.getLogger("WirelessBroker")
# Get the logger for logging the number of registered clients.
clientLogger = logging.getLogger("ClientOverview")

class WirelessBroker(WebSocket):
    # Check if the incoming device registration - via websocket - is legal.
    # Handle incoming messages.
    def handleMessage(self):
        logger.info("Enter: Handle websocket message.")
        # Wireless broker only allows device registrations via websockets.
        if self.data is not None and is_device_registration(self.data):
            # Registrating a new device.

            dict_payload = json.loads(self.data.decode("utf8"))

            # If this is a duplicate registration, delete the old registration and start a new.
            if dict_payload['client_id'] in socket_clienthandlers:
                del socket_clienthandlers[dict_payload['client_id']]

            # Add the socket handler.
            socket_clienthandlers[dict_payload['client_id']] = self
            # All was well, send the success response.
            response = "{\"message\": \"device_registration\", \"status\": \"OK\", \"response\": {\"ugc\": \"%s\", \"director\": \"%s\"}}" % (settings.ip["ugc"], settings.ip["director"])

            # Send the response.
            self.sendMessage(response)

            dict_payload = json.loads(self.data.decode("utf8"))
            logger.info("(from UE) Accepted websocket DEVICE REGISTRATION from %s.", dict_payload['client_id'])

            # Until no location is fetched, put it temporary to the unknown mac location.
            location[dict_payload['client_id']] = settings.wb["unknown_mac_location"]
            location_attempts[dict_payload['client_id']] = settings.wb["max_location_attempts"]

            clientLogger.info("New client registered: %s.", dict_payload['client_id'])
            log_clients_overview()
        elif self.data is not None and is_response_content_request(self.data):
        # Handle content request responses
             # Forward to UGC.
             status, request_id, content_id = getContentResponseInformation(self.data)
             # TODO: the state should depend on the status code from the UE's response.
             response_state = "failed"
             if status == "OK":
                response_state = "uploading"
             else:
                logger.error("(from UE) Status of incoming CONTENT REQUEST RESPONSE from %s is status %s (request_id %s and content_id %s).", get_client_mac(self), status, request_id, content_id)
             logger.info("(from UE) Call is CONTENT REQUEST RESPONSE from %s (status %s, request_id %s and content_id %s).", get_client_mac(self), status, request_id, content_id)
             forward_request_confirmation(request_id, content_id, response_state)
        elif self.data is not None and is_response_content_cancel(self.data):
        # Handle content cancel responses
             status, request_id, content_id = getContentResponseInformation(self.data)
             response_state = "failed"
             if status == "OK":
                response_state = "canceled"
             else:
                logger.error("(from UE) Status of incoming CONTENT CANCEL RESPONSE from %s is status %s (request_id %s and content_id %s).", get_client_mac(self), status, request_id, content_id)
             logger.info("(from UE) Call is CONTENT CANCEL RESPONSE from %s (status %s, request_id %s and content_id %s).", get_client_mac(self), status, request_id, content_id)
             # still has to be implemented at the UGC?
             forward_cancel_confirmation(request_id, content_id, response_state)
        elif self.data is not None and is_response_device_reconfig(self.data):
        # Handle device reconfig responses
             logger.info("(from UE) Call is DEVICE RECONFIG RESPONSE from %s.", get_client_mac(self))
             # still has to implement a response RESTful endpoint at the UGC?
        else:
            logger.error("(from UE) Wrong websocket server call.")
            # The call wasn't a legal device registration.
            # The only calls that can come in - on the websocket - are (legal) device registrations.
            fail_msg = "{'status':'FAILED'}"
            # Convert it to JSON.
            json_msg = json.dumps(fail_msg)
            self.sendMessage(json_msg)
        logger.info("Exit: Handle websocket message.")

    # Send something to the client.
    # @param payload The data to be send to the "to" paramater.
    # @param to The mac address to which the payload has to be send, only used for debugging.
    def sendClientMessage(self, payload, to):
        logger.info("Enter: Send a websocket message to %s.", to)
        self.sendMessage(payload)
        logger.debug("The sent websocket message: %s to %s.", payload, to)
        logger.info("Exit: Send a websocket message to %s.", to)

    # Handles incoming connections.
    def handleConnected(self):
        logger.info("Enter: New connection.")
        logger.info("%s connected.", self.address)
        logger.info("Exit: New connection.")

    # Handles closing connections.
    def handleClose(self):
        logger.info("Enter: Closed connection for address %s.", self.address)
        client_id = get_client_mac(self)
        logger.info("Cleaning connection of client %s.", client_id)
        
        if client_id in location.keys():
            del location[client_id]
        if client_id in location_attempts.keys():
            del location_attempts[client_id]
        logger.info("Cleaned client %s.", client_id)

        clientLogger.info("Disconnected client: %s.", client_id)
        log_clients_overview()

        print self.address, 'closed'
        logger.info("Exit: Closed connection for address %s.", self.address)

        if client_id in socket_clienthandlers.keys():
            del socket_clienthandlers[client_id]


# Test if the given data is json or not.
# @param myjson The data that is being checked.
# @return True or false if the data is json.
def is_json(myjson):
    try:
        json_object = json.loads(myjson)
    except ValueError, e:
        return False
    return True

# Returns the mac address of the handler that is given as a parameter.
# @param handler This is the socket handler of the particular client for which the mac address is wanted.
# @return The mac address of the given socket handler, if found.
def get_client_mac(handler):
    for key, val in socket_clienthandlers.iteritems():
        if val == handler:
            return key

# Check if the websocket handler exists for the given mac address exists.
# @param macaddress The mac address for which we want to check if the websocket handler exists.
# @return True if the socket handler exists, False otherwise.
def clienthandler_exist(macaddress):
    # socket_clienthandlers[macaddress] = handler
    if macaddress in socket_clienthandlers:
        logger.info("In function clienthandler_exist: client handler for mac %s is already registered.", macaddress)
        return True
    else:
        logger.info("In function clienthandler_exist: client handler for mac %s not registered.", macaddress)
        return False

# Writes all the current registered clients to the clientLogger.
def log_clients_overview():
    clientLogger.info("Total number of registered clients: %s.", len(socket_clienthandlers))

    if len(socket_clienthandlers) > 0:
        clients = ""
        for client, val in socket_clienthandlers.iteritems():
            clients = clients + client + "\r\n"
        clientLogger.info(clients)

# Check if the incoming device registration - via websocket - is legal.
# @param payload The data that is being checked for it being valid.
# @return Returns only True if the data is a device registration call, otherwise False.
def is_device_registration(payload):
    logger.debug("Enter: (from UE) is device registration check call.")

    # Check if the content - which is now a string - contains legal json.
    if not is_json(payload.decode("utf8")):
        logger.error("(from UE) In function is_device_registration: json check failed.")
        logger.debug("Exit: (from UE) is device registration check call.")
        return False
    # Convert to Python dictionary.
    dict_payload = json.loads(payload.decode("utf8"))
    if 'message' not in dict_payload or type(dict_payload['message']) is not unicode:
        logger.error("(from UE) In function is_device_registration: message check failed for %s.", dict_payload)
        logger.debug("Exit: (from UE) is device registration check call.")
        return False

    if dict_payload['message'] == 'device_registration':
        logger.debug("(from UE) In function is_device_registration: confirmed is device registration for %s.", dict_payload)
        if 'client_id' not in dict_payload or type(dict_payload['client_id']) is not unicode:
            logger.debug("(from UE) In function is_device_registration: client_id check failed for %s.", dict_payload)
            logger.debug("Exit: (from UE) is device registration check call.")
            return False
        if 'cameraResolution' not in dict_payload or type(dict_payload['cameraResolution']) is not unicode:
            logger.debug("(from UE) In function is_device_registration: cameraResolution check failed for %s.", dict_payload)
            logger.debug("Exit: (from UE) is device registration check call.")
            return False
        if 'cpu' not in dict_payload or type(dict_payload['cpu']) is not unicode:
            logger.debug("(from UE) In function is_device_registration: cpu check failed for %s.", dict_payload)
            logger.debug("Exit: (from UE) is device registration check call.")
            return False
        if 'user-agent' not in dict_payload or type(dict_payload['user-agent']) is not unicode:
            logger.debug("(from UE) In function is_device_registration: user-agent check failed for %s.", dict_payload)
            logger.debug("Exit: (from UE) is device registration check call.")
            return False
    else:
        logger.debug("(from UE) In function is_device_registration: not correct message type for %s.", dict_payload)
        logger.debug("Exit: (from UE) is device registration check call.")
        return False

    logger.debug("Exit: (from UE) is device registration check call.")
    # All was well, send true.
    return True

# Check if the incoming content request response - via websocket - is legal.
# @param payload The data that is being checked for it being valid.
# @return Returns only True if the data is a content request response, otherwise False.
def is_response_content_request(payload):
    logger.debug("Enter: (from UE) response content request check call.")

    # Check if the content - which is now a string - contains legal json.
    if not is_json(payload.decode("utf8")):
        logger.error("(from UE) In function is_response_content_request: json check failed.")
        logger.debug("Exit: (from UE) response content request check call.")
        return False
    # Convert to Python dictionary.
    dict_payload = json.loads(payload.decode("utf8"))
    if 'message' not in dict_payload or type(dict_payload['message']) is not unicode:
        logger.error("(from UE) In function is_response_content_request: message check failed for %s.", dict_payload)
        logger.debug("Exit: (from UE) response content request check call.")
        return False

    if dict_payload['message'] == 'content_request':
        logger.debug("(from UE) In function is_response_content_request: confirmed content request response for %s.", dict_payload)
        if 'status' not in dict_payload or type(dict_payload['status']) is not unicode:
            logger.debug("(from UE) In function is_response_content_request: status check failed for %s.", dict_payload)
            logger.debug("Exit: (from UE) response content request check call.")
            return False
        if 'response' not in dict_payload or type(dict_payload['response']) is not dict:
            logger.debug("(from UE) In function is_response_content_request: response check failed for %s.", dict_payload)
            logger.debug("Exit: (from UE) response content request check call.")
            return False
    else:
        logger.debug("(from UE) In function is_response_content_request: not correct message type for %s.", dict_payload)
        logger.debug("Exit: (from UE) response content request check call.")
        return False

    logger.debug("Exit: (from UE) response content request check call.")
    # All was well, send true.
    return True

# Returns the state, request id and content id of a content related call.
# @param payload The payload from which the data is retrieved.
# @return state The state, the request id and content id of the content related call.
def getContentResponseInformation(payload):
    logger.debug("Enter: Getting request and content id.")
    dict_payload = json.loads(payload.decode("utf8"))

    state = dict_payload["status"]
    request_id = dict_payload["response"]["request_id"]
    content_id = dict_payload["response"]["content_id"]
    logger.debug("Exit: Getting request %s and content id %s (status %s).", request_id, content_id, state)
    return state, request_id, content_id

# Check if the incoming content request cancel - via websocket - is legal.
# @param payload The data that is being checked for it being valid.
# @return Returns only True if the data is a content cancel response, otherwise False.
def is_response_content_cancel(payload):
    logger.debug("Enter: (from UE) response content cancel check call.")

    # Check if the content - which is now a string - contains legal json.
    if not is_json(payload.decode("utf8")):
        logger.error("(from UE) In function is_response_content_cancel: json check failed.")
        logger.debug("Exit: (from UE) response content cancel check call.")
        return False
    # Convert to Python dictionary.
    dict_payload = json.loads(payload.decode("utf8"))
    if 'message' not in dict_payload or type(dict_payload['message']) is not unicode:
        logger.error("(from UE) In function is_response_content_cancel: message check failed for %s.", dict_payload)
        logger.debug("Exit: (from UE) response content cancel check call.")
        return False

    if dict_payload['message'] == 'content_cancel':
        logger.debug("(from UE) In function is_response_content_cancel: confirmed content cancel response.")
        if 'status' not in dict_payload or type(dict_payload['status']) is not unicode:
            logger.debug("(from UE) In function is_response_content_cancel: status check failed for %s.", dict_payload)
            logger.debug("Exit: (from UE) response content cancel check call.")
            return False
        if 'response' not in dict_payload or type(dict_payload['response']) is not dict:
            logger.debug("(from UE) In function is_response_content_cancel: response check failed for %s.", dict_payload)
            logger.debug("Exit: (from UE) response content cancel check call.")
            return False
    else:
        logger.debug("(from UE) In function is_response_content_cancel: not correct message type for %s.", dict_payload)
        logger.debug("Exit: (from UE) response content cancel check call.")
        return False

    logger.debug("Exit: (from UE) response content cancel check call.")
    # All was well, send true.
    return True

# Check if the incoming device reconfig response - via websocket - is legal.
# @param payload The data that is being checked for it being valid.
# @return Returns only True if the data is a device reconfig response, otherwise False.
def is_response_device_reconfig(payload):
    logger.debug("Enter: (from UE) response device reconfig check call.")

    # Check if the content - which is now a string - contains legal json.
    if not is_json(payload.decode("utf8")):
        logger.error("(from UE) In function is_response_device_reconfig: json check failed.")
        logger.debug("Exit: (from UE) response device reconfig check call.")
        return False
    # Convert to Python dictionary.
    dict_payload = json.loads(payload.decode("utf8"))
    if 'message' not in dict_payload or type(dict_payload['message']) is not unicode:
        logger.error("(from UE) In function is_response_device_reconfig: message check failed for %s.", dict_payload)
        logger.debug("Exit: (from UE) response device reconfig check call.")
        return False

    if dict_payload['message'] == 'device_reconfig':
        logger.debug("(from UE) In function is_response_device_reconfig: confirmed device reconfig response.")
        if 'status' not in dict_payload or type(dict_payload['status']) is not unicode:
            logger.debug("(from UE) In function is_response_device_reconfig: status check failed for %s.", dict_payload)
            logger.debug("Exit: (from UE) response device reconfig check call.")
            return False
        if 'response' not in dict_payload or type(dict_payload['response']) is not dict:
            logger.debug("(from UE) In function is_response_device_reconfig: response check failed for %s.", dict_payload)
            logger.debug("Exit: (from UE) response device reconfig check call.")
            return False
    else:
        logger.debug("(from UE) In function is_response_device_reconfig: not correct message type for %s.", dict_payload)
        logger.debug("Exit: (from UE) response device reconfig check call.")
        return False

    logger.debug("Exit: (from UE) response device reconfig check call.")
    # All was well, send true.
    return True

# Send a asynchronous request confirmation message to the UGC when the WB received a content request response from a client UE.
# @param request_id The request id for which the confirmation has to be sent (has to be combined with the content_id)
# @param content_id The content id for which the confirmation has to be sent (has to be combined with the request_id)
# @param state The state that has to be sent in the confirmation message.
def forward_request_confirmation(request_id, content_id, state):
    logger.info("Enter: confirm request call (request id %s, content id %s) to UGC.", request_id, content_id)
    addr = settings.ip["ugc"] + "/v1/request/confirmation"
    try:
        payload = {'request_id': str(request_id), 'content_id': str(content_id), 'status': str(state)}
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        r = requests.post(addr, data=json.dumps(payload), headers=headers, timeout=settings.wb["max_request_timeout"])
        json_response = r.json()
        if (r.status_code != 200):
            raise Exception("Status code of call is NOT 200, but " + str(r.status_code))
        if ('status' not in json_response or 'response' not in json_response or type(json_response["response"]) is not dict):
           raise Exception("Response of confirm request RESTFUL had the wrong syntax: " + str(json_response))
        if (json_response["status"] != 'OK'):
           raise Exception("Response of confirm request RESTFUL contained a status NOT OK: " + str(json_response))
        logger.info("Exit: confirm request call (request id %s, content id %s) to UGC.", request_id, content_id)
    except Exception as detail:
        logger.exception("Exit: confirm request call to UGC - could not send a confirmation request (request_id %s, content_id %s and state %s) to UGC (%s): %s.", request_id, content_id, state, settings.ip["ugc"] ,detail)


# Send a asynchronous cancel request confirmation message to the UGC when the WB received a cancel request response from a client UE.
# @param request_id The request id for which the confirmation has to be sent (has to be combined with the content_id)
# @param content_id The content id for which the confirmation has to be sent (has to be combined with the request_id)
# @param state The state that has to be sent in the confirmation message.
def forward_cancel_confirmation(request_id, content_id, state):
    logger.info("Enter: confirm cancel request call (request id %s, content id %s) to UGC.", request_id, content_id)
    addr = settings.ip["ugc"] + "/v1/request/cancel"
    try:
        payload = {'request_id': str(request_id), 'content_id': str(content_id), 'status': str(state)}
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        r = requests.post(addr, data=json.dumps(payload), headers=headers, timeout=settings.wb["max_request_timeout"])
        json_response = r.json()
        if (r.status_code != 200):
            raise Exception("Status code of call is NOT 200, but " + str(r.status_code))
        if ('status' not in json_response or 'response' not in json_response or type(json_response["response"]) is not dict):
           raise Exception("Response of confirm cancel request RESTFUL had the wrong syntax: " + str(json_response))
        if (json_response["status"] != 'OK'):
           raise Exception("Response of confirm cancel request RESTFUL contained a status NOT OK: " + str(json_response))
        logger.info("Exit: confirm cancel request call (request id %s, content id %s) to UGC.", request_id, content_id)
    except Exception as detail:
        logger.exception("Exit: confirm cancel request call to UGC - could not send a confirmation cancel request (request_id %s, content_id %s and state %s) to UGC (%s): %s.", request_id, content_id, state, settings.ip["ugc"] ,detail)

################ WebSocket forwards to UE ##################

# Forward content request payload to the UE websocket and adapt it for the UE.
# @param payload The content request data that has to be forwarded to the UE.
def fwd_content_request(payload):
    logger.info("Enter: (to UE) forward content request call for client_id %s.", payload["client_id"])
    sockethandler = socket_clienthandlers.get(payload["client_id"])
    payload["message"] = "content_request"
    del payload["deadlinePolicy"]
    del payload["deadline"]
    payload["SendStartTime"] = "0"
    payload["sendRate"] = "54Mbps"
    strpayload = json.dumps(payload).encode('utf8')
    # Send it to the UE's websocket.
    sockethandler.sendClientMessage(strpayload, payload["client_id"])
    logger.info("Exit: (to UE) forward content request call for client_id %s.", payload["client_id"])

# Forward content cancel payload to the UE websocket.
# @param payload The content cancel data that has to be forwarded to the UE.
def fwd_content_cancel(payload):
    logger.info("Enter: (to UE) forward content request call for client_id %s.", payload["client_id"])
    sockethandler = socket_clienthandlers.get(payload["client_id"])
    payload["message"] = "content_cancel"
    strpayload = json.dumps(payload).encode('utf8')
    # Send it to the UE's websocket.
    sockethandler.sendClientMessage(strpayload, payload["client_id"])
    logger.info("Exit: (to UE) forward content request callfor client_id %s.", payload["client_id"])

# Forward device reconfig payload to UE websocket.
# @param payload The device reconfig data that has to be forwarded to the UE.
# @param client_id The id of the client to identify to which UE it has to be sent.
def fwd_device_reconfig(payload, client_id):
    logger.info("Enter: (to UE) forward device reconfiguration call for client_id %s.", client_id)
    sockethandler = socket_clienthandlers.get(client_id)
    payload["message"] = "device_reconfig"
    strpayload = json.dumps(payload).encode('utf8')
    # Send it to the UE's websocket.
    sockethandler.sendClientMessage(strpayload, client_id)
    logger.info("Exit: (to UE) forward device reconfiguration call for client_id %s.", client_id)