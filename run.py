import thread
import time
import logging
import os
import datetime
import settings
from WBRestful import app
from WBController import update_locations, setup_ssh_connection
from WBWebSocket import SimpleWebSocketServer, WirelessBroker

## Setup logging.

## Set up the wireless broker logger.
logger = logging.getLogger("WirelessBroker")
logger.propagate = False
formatter = logging.Formatter('%(asctime)s - %(name)s - %(module)s - %(levelname)s - %(message)s')
# create file handler which logs even debug messages
experiment_directory = settings.wb["experiment_directory"]
if not os.path.exists(experiment_directory):
    os.makedirs(experiment_directory)
datetimestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H-%M-%S')

experiment_file = experiment_directory + "/WB-" + str(datetimestamp) + ".log"
error_experiment_file = experiment_directory + "/error-WB-" + str(datetimestamp) + ".log"

fh = logging.FileHandler(experiment_file)
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
eh = logging.FileHandler(error_experiment_file)
eh.setLevel(logging.ERROR)
eh.setFormatter(formatter)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
ch.setFormatter(formatter)
logger.addHandler(fh)
logger.addHandler(eh)
logger.addHandler(ch)

# client overview logger
clientLogger = logging.getLogger("ClientOverview")
clientLogger.propagate = False
formatter = logging.Formatter('%(asctime)s - %(name)s - %(module)s - %(levelname)s - %(message)s')

client_experiment_file = experiment_directory + "/clients-WB-" + str(datetimestamp) + ".log"

clienth = logging.FileHandler(client_experiment_file)
clienth.setLevel(logging.DEBUG)
clienth.setFormatter(formatter)
clientLogger.addHandler(clienth)

# Set the logging level of Paramiko
logging.getLogger("paramiko").setLevel(logging.WARNING)

## End setting up logging.

# Start the restful server.
def start_restfulserver():
    if settings.public:
	    # public
        app.run(host=settings.ip["wb"], port=settings.wb["port_restful"])
    else:
        # local
        app.run(debug=False, port=settings.wb["port_restful"])

# Start updating the locations periodically.
def start_updating_locations():
    # Using the Meru APs
    ssh = setup_ssh_connection()
    code = 0
    while True:
        while (ssh is None or code == 2):
            if (ssh is not None):
                ssh.close()
            ssh = setup_ssh_connection()
            code = 0
            logger.error("SSH connection failed for periodical location updates. Retrying.")
        code = update_locations(ssh)
        time.sleep(settings.wb["location_update_interval"])

# Test the connection to the controller, throw an exception if it fails.
def test_connections():
    logger.info("Enter: Testing external connections.")
    try:
        ssh = setup_ssh_connection()
        if (ssh != None):
            logger.info("SSH connection successfully tested.")
            ssh.close()
            ssh = None
            logger.info("Exit: Testing external connections.")
        else:
            raise Exception("Not able to set up SSH connection.")
    except Exception as detail:
        logger.exception("Exit: Testing external connections - Exception: %s", detail)

# Run this only if the script is ran directly.
if __name__ == '__main__':
    try:
        if not settings.airtight:
            test_connections()
        thread.start_new_thread(start_restfulserver, ())
        thread.start_new_thread(start_updating_locations, ())
        if settings.public:
            # public
            server = SimpleWebSocketServer(settings.ip["wb"], settings.wb["port_websockets"], WirelessBroker)
            server.serveforever()
        else:
            # local
            server = SimpleWebSocketServer('', settings.wb["port_websockets"], WirelessBroker)
            server.serveforever()
    except Exception, err:
        logger.exception("Caught exception: %s", err)
